//
//  ThumbWheel.swift
//  ThumbWheel
//
//  Created by Salvatore  Polito on 27/07/17.
//  Copyright © 2017 Salvatore  Polito. All rights reserved.
//

import UIKit

class ThumbWheel: UIControl {
    /// Value property indicates the wheel position
    ///
    /// To observe user interaction (scrolling the wheel)
    /// use a KVO observer.
    /// To manually adjust the wheel's position change this.
    @objc public var value : Int {
        didSet {
            if value != internalPosition {
                position = value
            }
        }
    }
    
    //Used for external inputs
    private var position : Int {
        didSet {
            scrollView.changePosition(newPosition: position)
        }
    }
    
    //Used to observe internal inputs (scrolling)
    private var internalPosition: Int {
        didSet {
            setValue(internalPosition, forKey: "value")
        }
    }
    
    private let arrowWidth: CGFloat = 20
    
    private var scrollView: ThumbScrollView
    
    init(frame: CGRect, tickNumber: Int) {
        let scrollViewFrame = CGRect(x: arrowWidth, y: 0, width: frame.width - (arrowWidth * 2), height: 20)
        scrollView = ThumbScrollView(frame: scrollViewFrame, tickNumber: tickNumber)
        
        internalPosition = scrollView.position
        value = internalPosition
        position = internalPosition
        
        super.init(frame: frame)
        backgroundColor = .clear
        
        scrollView.center = self.center
        addSubview(scrollView)
        
        let indicator = drawIndicator()
        let indicatorSize = CGSize(width: 2, height: 30)
        let indicatorOrigin = CGPoint(x: self.frame.width/2 - indicatorSize.width/2, y: self.frame.height/2 - indicatorSize.height/2)
        indicator.frame = CGRect(origin: indicatorOrigin, size: indicatorSize)
        self.layer.addSublayer(indicator)
        
        let downArrow = Arrow(frame: CGRect(x: arrowWidth/2, y: 1, width: arrowWidth, height: arrowWidth))
        downArrow.backgroundColor = .clear
        downArrow.rotationAngle = .pi - .pi/4
        addSubview(downArrow)
        
        let upArrow = Arrow(frame: CGRect(x: scrollView.frame.maxX - arrowWidth/2, y: 1, width: arrowWidth, height: arrowWidth))
        upArrow.backgroundColor = .clear
        upArrow.rotationAngle = -.pi/4
        addSubview(upArrow)
        
        
        scrollView.addObserver(self, forKeyPath: #keyPath(ThumbScrollView.position), options: .new, context: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        fatalError("inti(frame:) has not been implemented")
    }
    
}

extension ThumbWheel {
    //MARK: - DRAWING UTILITIES
    private func drawIndicator() -> CAShapeLayer{
        let layer = CAShapeLayer()
        
        layer.masksToBounds = true
        let bezierPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 1, height: 30))
        layer.fillColor = UIColor.yellow.cgColor
        layer.lineWidth = 1
        layer.strokeColor = UIColor.yellow.cgColor
        
        layer.path = bezierPath.cgPath
        
        return layer
    }
    
    //MARK: - Observer override
    override internal func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath! == #keyPath(ThumbScrollView.position) {
            self.internalPosition = scrollView.position
        }
    }
}


fileprivate class ThumbScrollView: UIScrollView, UIScrollViewDelegate {
    //mantaining thumb relative position
    @objc public var position : Int = 0
    
    private var internalViewFrameSize : CGSize
    private var internalViewFrame : CGRect
    
    private var tickDistance : CGFloat = 20
    private var tickNumber : Int = 30
    
    private var padding : CGFloat
    private let arrowWidth : CGFloat = 20
    
    init(frame: CGRect, tickNumber: Int) {
        //internal view has to be 2*scrollViewWidth
        internalViewFrameSize = CGSize(width: frame.width * 2, height: 20)
        internalViewFrame = CGRect(origin: .zero, size: internalViewFrameSize)
        
        //To centrate the thumb we need half of frameWidth padding
        padding = frame.width/2
        self.tickNumber = tickNumber
        tickDistance = internalViewFrameSize.width / CGFloat(tickNumber)
        
        super.init(frame: frame)
        
        //Customize scrollview
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        //scrollView.decelerationRate = UIScrollViewDecelerationRateFast
        self.delaysContentTouches = false
        self.autoresizingMask = .flexibleWidth
        self.bounces = false
        self.delegate = self
        self.backgroundColor = .clear
        
        //Customize scrollview's content
        let internalView = setupInternalView()
        self.contentSize = internalView.frame.size
        self.contentInset = UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
        self.contentOffset = CGPoint(x: internalView.bounds.width/2 - padding , y: 0)
        self.addSubview(internalView)
        
        
        let newPosition = Int((self.contentOffset.x + padding) / tickDistance)
        self.position = newPosition
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (timer) in
            self.setPosition(newPosition: newPosition)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        fatalError("inti(frame:) has not been implemented")
    }
    
    private func setPosition(newPosition: Int) {
        //I think this check is useless
        var position = newPosition
        if newPosition < 0 {
            position = 0
        } else if newPosition > tickNumber {
            position = tickNumber
        }
        
        self.position = position
        setValue(position, forKeyPath: "position")
    }
    
    func changePosition(newPosition: Int) {
        if newPosition == self.position {
            return
        }
        //I think this check is useless
        var position = newPosition
        if newPosition < 0 {
            position = 0
        } else if newPosition > tickNumber {
            position = tickNumber
        }
        
        let x = position * Int(tickDistance) - Int(padding)
        let positionPoint = CGPoint(x: x, y: 0)
        self.setContentOffset(positionPoint, animated: true)
        
       //Not needed as we use changePosition() externally,
       //and we don't want to notify position change
       //self.position = position
       //setValue(position, forKeyPath: "position")
    }
    
    //MARK: - DELEGATES OVERRIDE
    
    //Called every time user initiate a scrolling gesture
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let newPosition = Int((scrollView.contentOffset.x + padding) / tickDistance)
        setPosition(newPosition: newPosition)
    }
    
    //Called when user cancel the scroll gesture at some point
    //we want to adjust the wheel
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            let x = position * Int(tickDistance) - Int(padding)
            let positionPoint = CGPoint(x: x, y: 0)
            scrollView.setContentOffset(positionPoint, animated: true)
        }
    }
    
    //Called when user do a long scroll
    //we want to adjust the thumb position at the decelaration animation's end
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = position * Int(tickDistance) - Int(padding)
        let positionPoint = CGPoint(x: x, y: 0)
        scrollView.setContentOffset(positionPoint, animated: true)
    }
}

extension ThumbScrollView {
    private func setupInternalView() -> UIView {
        //Internal View
        let internalView = UIView(frame: internalViewFrame)
        internalView.backgroundColor = .clear
        internalView.clipsToBounds = true
        let layer = drawBackground()
        layer.frame = CGRect(x: 0, y: 2, width: internalViewFrameSize.width, height: internalViewFrameSize.height)
        internalView.layer.addSublayer(layer)
        
        return internalView
    }
    
    //MARK: - DRAWING UTILITIES
    private func drawBackground() -> CAShapeLayer{
        let layer = CAShapeLayer()
        
        layer.masksToBounds = true
        let bezierPath = UIBezierPath()
        layer.fillColor = UIColor.clear.cgColor
        layer.lineWidth = 1
        layer.strokeColor = UIColor.white.cgColor
        
        var startPoint: CGPoint
        var endPoint: CGPoint
        
        var count = 0
        let tickDistance = Int(self.tickDistance)
        let tick = Int(internalViewFrameSize.width / self.tickDistance)
        let shortTickWidth = 12
        let longTickWidth = 20
        
        for i in 0...tick {
            if count == 0 {
                count = 4
                startPoint = CGPoint(x: i*tickDistance, y: 0)
                endPoint = CGPoint(x: i*tickDistance, y: longTickWidth)
                bezierPath.move(to: startPoint)
                bezierPath.addLine(to: endPoint)
            } else {
                count -= 1
                startPoint = CGPoint(x: i*tickDistance, y: 4)
                endPoint = CGPoint(x: i*tickDistance, y: shortTickWidth)
                bezierPath.move(to: startPoint)
                bezierPath.addLine(to: endPoint)
            }
        }
        layer.path = bezierPath.cgPath
        
        return layer
    }
}
