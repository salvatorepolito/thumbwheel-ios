//
//  ThumbWheel.swift
//  ThumbWheel
//
//  Created by Salvatore  Polito on 27/07/17.
//  Copyright © 2017 Salvatore  Polito. All rights reserved.
//

import UIKit

@IBDesignable
class Arrow: UIView {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    @IBInspectable var rotationAngle : CGFloat = 0 {
        didSet {
            self.layoutIfNeeded()
        }
    }
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let lineWidth = frame.width * sqrt(2) - 1
        
        //// arrow Drawing
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x: 0, y: lineWidth))
        arrowPath.addLine(to: CGPoint(x: lineWidth, y: 0))
        arrowPath.addLine(to: CGPoint(x: lineWidth, y: lineWidth))
        arrowPath.addLine(to: CGPoint(x: 0, y: lineWidth))
        UIColor.white.setFill()
        arrowPath.fill()
        UIColor.white.setStroke()
        arrowPath.lineWidth = 1
       // arrowPath.lineJoinStyle = .round
        arrowPath.stroke()
        
        self.transform = CGAffineTransform(rotationAngle: rotationAngle)
    }
    
}
