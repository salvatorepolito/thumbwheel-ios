//
//  ViewController.swift
//  ThumbWheel
//
//  Created by Salvatore  Polito on 25/07/17.
//  Copyright © 2017 Salvatore  Polito. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!

    let wheel = ThumbWheel(frame: CGRect(x: 0, y: 0, width: 300, height: 20), tickNumber: 20)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        
        wheel.center = view.center
        wheel.addObserver(self, forKeyPath: #keyPath(ThumbWheel.value), options: .new, context: nil)
        
        view.addSubview(wheel)
        
        Timer.scheduledTimer(withTimeInterval: 4, repeats: false) { (timer) in
            self.wheel.value = 1
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath! == #keyPath(ThumbWheel.value) {
            print(wheel.value)
            label.text = "\(wheel.value)"
        }
    }
}

